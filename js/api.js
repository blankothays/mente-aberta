var server = "https://menteaberta.azurewebsites.net/"
//var server = "https://localhost:44300/"

// var loading =  document.createElement('div');
// loading.className = "transition";

// document.body.appendChild(loading);

var contentClick;
$('input.nova-senha').keyup(function(){
	var senha = $('input[name="senhaUsuario"]');
	var confereSenha = $('input[name="confereSenhaUsuario"]');
	if( confereSenha.val() != ''){
		if ( confereSenha.val() == senha.val()){
			confereSenha.closest('.form-group').addClass('valid');
			$('.validation').text('');
		}else{
			confereSenha.closest('.form-group').removeClass('valid');
		}
	}
	$(this).parent().siblings('.confirm').show();
});

$('.form-group:not(.valid) .edit').click(function(){
	$(this).parent().removeClass('disabled');
	$(this).siblings('input').prop('disabled', false);
	$(this).siblings('input[type="password"]').val('');
});

$('.user-name a').text(sessionStorage.getItem('userName'));

$('.done').click(function(){
	var contentId = $(this).attr('content-id');
	var userId = sessionStorage.getItem('userId');
	SetAsDone(contentId, userId);
	$(this).removeClass('un');
	$('[data-content="'+ contentId +'"]').addClass('done');
});

$('.send-comment').click(function(e){
	e.preventDefault();
	var contentId = $(this).attr('content-id');
	var userId = sessionStorage.getItem('userId');
	var msg = $('.comments textarea').val();

	SendComment(contentId, userId, msg);
});

var headers = {
	"Content-Type" : "application/json",
	"Authorization" : 'Bearer ' + sessionStorage.getItem('access_token')
};

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function login(){
	var email = $('input[name="emailLogin"]').val();
	var senha = $('input[name="senhaLogin"]').val();
	var data = {"grant_type":"password","username":email,"password":senha};
	
	$.ajax({
		url: server + 'Token',
		method: "POST",
		data : data,
		success : function(response){
			sessionStorage.setItem('userId', response.UsuarioId);
			sessionStorage.setItem('access_token', response.access_token);
			headers.Authorization = 'Bearer ' + response.access_token;
			getUser('cursos.html');
		},
		error : function(error){
			$('.validation').text('Usuário ou senha incorretos').removeClass('success').addClass('error');
		}
	});
}

function register(){
	var nome = $('input[name="nomeUsuario"]').val();
	var email = $('input[name="emailUsuario"]').val();
	var senha = $('input[name="senhaUsuario"]').val();
	var confereSenha = $('input[name="confereSenhaUsuario"]').val();
	if(nome != '' && email != '' && senha != '' && confereSenha == senha && senha.length >= 6){
		var data = {
			"NomeUsuario": nome,
			"Email": email,
			"Tipo": 4,
			"Password": senha,
			"ConfirmPassword": confereSenha
		};
		$.ajax({
			url: server + "api/v1/Account/RegisterUser/",
			method: "POST",
			data: data,
			success : function(response){
				$('.validation').html('Cadastro realizado com sucesso. <a href="login.html">Faça o login para entrar.</a>').removeClass('error').addClass('success');
				$('.transition').removeClass('active');
			},
			error : function(error){
				$('.validation').text(error.responseJSON.ModelState[""].$values[0]).removeClass('success').addClass('error');
			}
		});
	}else{
		if( senha != confereSenha){
			$('.validation').text('As senhas informadas não conferem.').removeClass('success').addClass('error');
		}else if(senha.length < 6){
			$('.validation').text('A senha precisa conter mais de 6 caracteres.').removeClass('success').addClass('error');
		}else{
			$('.validation').text('Por favor, verifique todos os campos').removeClass('success').addClass('error');
		}
	}
}

function listCourses(){
	var id = sessionStorage.getItem('userId');
	$.ajax({
		url: server + "api/v1/Curso/GetByUsuario/" + id,
		type: "GET",
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function (response){
			[].forEach.call(response.result.$values, function(obj){
				var html = '<div class="col-sm-12 col-md-6 col-lg-4 col-wd-3 col-sw-2 clear grid-item"><div class="course"><div class="thumbnail relative"><img src="' + server + '' + obj.Foto +'"><a href="curso-interna.html?courseId=' + obj.CursoId + '"><div class="overlay absolute top left"><div class="box-center-xy">Conheça o curso</div></div></a></div><div class="details"><h4><a href="curso-interna.html?courseId=' + obj.CursoId + '">'+ obj.NomeCurso +'</a></h4><!--p>Prof. John Doe</p--></div></div></div>';
				$('.courses').append(html);
			});
			$('.transition').removeClass('active');
		}
	});
}

function listNews(){
	$.ajax({
		url: server + "api/v1/Noticia",
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			[].forEach.call(response.result.$values, function(obj){
				var html = '<div class="col-sm-12 col-md-6 col-lg-6 clear grid-item"><div class="news"><div class="thumbnail relative"><img src="' + server + '' + obj.Foto +'"></div><div class="details"><h2><a href="noticia-interna.html?id='+ obj.NoticiaId +'">'+ obj.Titulo +'</a></h2><p>'+ obj.Descricao.substr(0,119) +'...</p><a href="noticia-interna.html?id='+ obj.NoticiaId +'" class="btn m-t-20">Leia Mais</a></div></div></div>';				
				$('.news-container').append(html);
				var html = '<li><a href="noticia-interna.html?id='+ obj.NoticiaId +'"><div class="icon"></div><div class="inline-block vertical-middle">'+ obj.Titulo + '</div></a></li>';
				$('.last-news').append(html);
			});
			$('.transition').removeClass('active');
		}
	});
}

function listWeeks(courseId){
	$.ajax({
		url: server + "api/v1/Semana/GetCurso/"+courseId,
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			var arrayWeeks = response.result.$values;
			[].forEach.call(arrayWeeks, function(obj){
				var html = '<li data-semana="'+ obj.SemanaId +'">'+obj.NomeSemana+'</li>'
				$('.dropdown').append(html);
			});
			listContents(courseId, arrayWeeks[0].SemanaId);
			$('.dropdown li').on('click', function(){
				
				if(!$(this).hasClass('active')){
					var weekId = $(this).attr('data-semana');
					listContents(courseId, weekId);
					$('.transition').addClass('active');
				}
				$(this).addClass('active').siblings().removeClass('active');
				$(this).parent().toggleClass('open');
			});
			$('.dropdown li:first-of-type').addClass('active');
		}
	});
}

function getNews(id){
	String.prototype.replaceAll = function(search, replacement) {
		var target = this;
		return target.replace(new RegExp(search, 'g'), replacement);
	};

	$.ajax({
		url: server + "api/v1/Noticia/"+id,
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			if(response.Descricao){
				var descicao = response.Descricao.replace(/(?:\r\n|\r|\n)/g, "<br/>");
				$('.content p').html(descicao);
			}
			$('article h3').text(response.Titulo);
			$('.media img').attr('src', server + '' + response.Foto);
			listNews();
			$('.transition').removeClass('active');
		}
	});
}

function getUser(next){
	var id = sessionStorage.getItem('userId');
	$.ajax({
		url: server + "api/v1/Usuario/"+ id,
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			sessionStorage.setItem('userName', response.NomeUsuario);
			sessionStorage.setItem('userEmail', response.Email);
			window.location.replace(next);
		}
	});
}

function updateUser(){
	var fieldsChanged = false;
	var id = sessionStorage.getItem('userId');
	var data = {
		"UsuarioId": id,
	};
	if($('[name="nomeUsuario"]').prop('disabled') == false){
		fieldsChanged = true;
		data.NomeUsuario = $('[name="nomeUsuario"]').val()
	}

	if( $('[name="senhaUsuario"]').val() != ''){
		fieldsChanged = true;
		data.Password = $('[name="senhaUsuario"]').val();
		data.ConfirmPassword = $('[name="confereSenhaUsuario"]').val();
	}

	data.Email = $('[name="emailUsuario"]').val();
	data.OldPassword = $('[name="senhaAntigaUsuario"]').val();

	if(fieldsChanged){
		if(data.Password == data.ConfirmPassword){
			$.ajax({
				url: server + "api/v1/Account/UpdateUser/",
				method : "POST",
				headers : headers,
				data : JSON.stringify(data),
				complete : function(){
			$('.transition').removeClass('active');
				},
				beforeSend : function(){
					$('.transition').addClass('active');
				},
				success : function(response){
					if(response.Status == false){
						$('.validation').text(response.Message).removeClass('success').addClass('error');
						if(data.Password != '' && data.OldPassword == ''){
							$('.validation').append(' Verifique se todos os campos estão preenchidos.');
						}
					}else{
						$('.validation').text('Dados atualizados com sucesso').removeClass('error').addClass('success');
					}
				},
				error : function (error) {
					$('.validation').text("Não foi possível atualizar os dados").removeClass('success').addClass('error');
				}
			});
		}else{
			$('.validation').text('As senhas informadas não conferem.').removeClass('success').addClass('error');
		}
	}else{
		$('.validation').text('Nada foi alterado').removeClass('error').addClass('success');
	}

}

function listContents(courseId, weekId){
    var userId = sessionStorage.getItem('userId');
	$.ajax({
		url: server + "api/v1/Conteudo/RetornaConteudoSemana/"+courseId+"/"+weekId,
		headers : headers,
		success : function(response){
			$('aside li').remove();
			[].forEach.call(response.result.$values, function(obj){
				var ul;
				switch(obj.TipoConteudo){
					case 1 :
						ul = $('aside .last-news');
						break;
					case 2 :
						ul = $('aside .audios');
						break;
					case 3 :
						ul = $('aside .videos');
						break;
				}
				html = '<li data-content="'+ obj.ConteudoId+'"><div class="icon"></div><div class="inline-block vertical-middle"><div class="title">'+ obj.Titulo +'</div></div></li>';
				checkIfIsDone(obj.ConteudoId, sessionStorage.getItem('userId'));
				ul.append(html);
			});
			if($('aside li').length > 0){
				var firstContent = $('aside li').first().addClass('active');
				var contentId = firstContent.attr('data-content');
				getContent(contentId, userId);
				//Access(contentId, userId);
				$('aside li').on('click', function(e){
					$(this).closest('aside').find('li').removeClass('active');
					$(this).addClass('active');
					var contentId = $(this).attr('data-content');
					getContent(contentId, userId);
					//Access(contentId, userId);
					e.stopImmediatePropagation();
    				return false;
				});
			}else{
				$('article').hide();
				$('.done').hide();
			}
			$('.transition').removeClass('active');
		}
	});
}

function getContent(id, userId){
	$.ajax({
		url: server + "api/v1/Conteudo/GetByUsuario/"+id +"/" + userId ,
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			switch(response.TipoConteudo){
				case 1 :
					$('.media .iframe-container').removeClass('audio-src').hide();
					$('.media img').show();
					$('article h3.title').text('Texto ' + response.Ordem);
					$('.media img').attr('src', server + '' + response.Foto);
					break;
				case 2 :
					$('.media img').hide();
					$('.media .iframe-container').addClass('audio-src').show();
					$('article h3.title').text('Áudio ' + response.Ordem);
					$('.media .iframe-container').html('<iframe class="audio-src" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='+ response.Url +'&amp;color=008ac0&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>');
					break;
				case 3 :
					$('.media img').hide();
					$('.media .iframe-container').removeClass('audio-src').show();
					$('article h3.title').text('Vídeo ' + response.Ordem);
					url = response.Url.replace('https://vimeo.com/', '')
					$('.media .iframe-container').html('<iframe src="https://player.vimeo.com/video/'+ url +'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>	')
					break;
			}
			$('article h3.title').text(response.Titulo);
			if(response.Descricao){
				var descicao = response.Descricao.replace(/(?:\r\n|\r|\n)/g, "<br/>");
				$('article .content p').html(descicao);
			}
			$('.done').show();
			$('.done').attr('content-id', id);

			var comment = "";
			if(response.comentario !== null){
                comment = response.comentario.Comentario;
            }else{
                comment = "";
			}
            $('.comments textarea').val(comment);
            $('.send-comment').attr('content-id', id);

            if($('[data-content="'+ id +'"]').hasClass('done') ){
				$('.done').removeClass('un');
			}	else{
				$('.done').addClass('un');
			}

			$('article').show();
			
			$('.transition').removeClass('active');
		},
		error : function(error){
		}
	});
}

function getCourse(id){
	$.ajax({
		url: server + "api/v1/Curso/"+id,
		headers : headers,
		beforeSend : function(){
			$('.transition').addClass('active');
		},
		success : function(response){
			if(response.Descricao){
				var descicao = response.Descricao.replace(/(?:\r\n|\r|\n)/g, "<br/>");
				$('.about-course').html(descicao);
			}
			
			$('.course-name').text(response.NomeCurso);
		}
	});	
}

function logout(){
	sessionStorage.removeItem('userEmail');
	sessionStorage.removeItem('userName');
	sessionStorage.removeItem('userId');
	sessionStorage.removeItem('access_token');
	window.location.replace('login.html');
}

function checkIfIsDone(contentId, userId){
	$.ajax({
		url: server + "api/v1/ExercicioFeito/RetornaExercicioFeito/"+ userId + '/' + contentId,
		headers : headers,
		success : function(response){
			if(response.Status === 'true'){
				$('[data-content="'+contentId+'"]').addClass('done');
			}else{
				$('[data-content="'+contentId+'"]').removeClass('done');
			}
		}
	});
}

function Access(contentId, userId){
	$.ajax({
		"async": true,
	  	"crossDomain": true,
		url: server + "api/v1/ExercicioFeito/ExercicioAcesso/" + userId + "/" + contentId,
	    type : "POST",
		headers : headers,
		success : function(response){
			//console.log("FOI", response);
		}
	});
}

function SetAsDone(contentId, userId){
	$.ajax({
		"async": true,
	  	"crossDomain": true,
		url: server + "api/v1/ExercicioFeito/ExercicioRealizado/"+ userId + "/" + contentId,
	    type : "POST",
		headers : headers
	});
}

function SendComment(contentId, userId, msg){
	var data = {
    	"UsuarioId" : userId,
    	"ConteudoId": contentId,
    	"Comentario": msg
	};

	$.ajax({
		url: server + "api/v1/UsuarioComentario/",
	    type : "POST",
		headers : headers,
		data : JSON.stringify(data),
		success : function(response){
			$('.comments .validation').fadeIn(300).delay(1000).fadeOut(300)
		}
	});
}

function redefinePassword(){
	var data = {
		Email : $('[name="emailEsqueciSenha"]').val(),
		Url : "https://menteabertasite.azurewebsites.net/redefinir-senha.html"
	};
	$.ajax({
		"async": true,
	  	"crossDomain": true,
		url: server + "api/v1/Account/SendResetPasswordEmail/",
	    type : "POST",
		data : data,
		success : function (response){
			$('.validation').text("Confira seu e-mail para redefinir sua senha.").removeClass('error').addClass('success');
		},
		error : function (error){
			$('.validation').text("Não foi possível enviar. Verifique se o e-mail digitado está correto.").removeClass('success').addClass('error');
		}
	});
}

function resetPassword(email, code){
	var data = {
	    Email : email,
	    Code : code,
	    Password : $('[name="confirmaNovaSenhaUsuario"]').val(),
	    ConfirmPassword : $('[name="novaSenhaUsuario"]').val()
	};
	$.ajax({
		"async": true,
	  	"crossDomain": true,
		url: server + "api/v1/Account/ResetPassword/",
	    type : "POST",
		data : data,
		success : function (response){
			$('.validation').html('Sua senha foi redefinida com sucesso. <a href="login.html">Faça o login para entrar.</a>').removeClass('error').addClass('success');
		},
		error : function (error){
			$('.validation').text("Não foi possível redefinir. Verifique se o link está correto.").removeClass('success').addClass('error');
		}
	});
}